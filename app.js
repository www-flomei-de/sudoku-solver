<<<<<<< HEAD
/*
    How to solve a Sudoku (programmatically)

    Sudokus have a simple ruleset to begin with:

    * the playing field is 9x9 fields in size
    * only numbers from 1 to 9 are allowed
    * each number must be unique to a row
    * each number must be unique to a col
    * each number must be unique to a quadrant (an 3x3 area)
     
    This ruleset can be used to build a list with allowed values 
    for each coordinate on the playing field in the beginning.

    If you find a unique value for a coordinate, enter it and
    re-run this list. This narrows down which numbers can/need
    to be placed.

    The next step is to look which values can only be placed in
    a unique position in a quadrant. As there's no other place
    for that value - but it needs to be present - this can be set, too.

    After having done those two steps till no more unique values
    are found, there's another step: You could look at a row/col and 
    quadrant combination. If there's a quadrant (q.0) with multiple 
    possible places for a value (p.0.0, p.2.1), say 5, and there's 
    another quadrant (q.1) where those places are all in a single 
    row/col (p.0.6, p.0.7), the unique placement options from q1 will 
    invalidate the placement option p.0.0 from q.0.

    This rule, which is "rather easy" to do by hand is skipped in
    this implementation. Instead it focuses directly on bruteforcing
    to find the missing placements.

*/

/* will hold the game (or the values of it) */
var dataset = [
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],

    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],

    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],]
]

/* this is a dummy dataset for testing */
/*
var dataset = [
    [0,0,0, 0,0,7, 9,0,0],
    [0,0,0, 0,0,0, 0,0,0],
    [0,9,1, 2,0,5, 0,8,0],

    [0,0,0, 4,0,0, 0,2,0],
    [2,7,0, 0,8,3, 4,0,9],
    [0,1,5, 0,0,0, 0,0,3],

    [0,0,9, 0,0,0, 0,0,0],
    [0,5,4, 0,0,2, 0,0,1],
    [0,0,0, 5,7,8, 0,0,0]
]
*/

/* 
    define the lower and upper limits for looping over quadrant positions
    these are coordinate pairs: [row, row], [col, col]
*/
const quadrants = [
    [[0,2], [0,2]],
    [[0,2], [3,5]],
    [[0,2], [6,8]],
    
    [[3,5], [0,2]],
    [[3,5], [3,5]],
    [[3,5], [6,8]],
    
    [[6,8], [0,2]],
    [[6,8], [3,5]],
    [[6,8], [6,8]]
]

/* structure to hold probable values */
var probableValues = [
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],

    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],

    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],]
]



document.addEventListener("DOMContentLoaded", (event) => {
    
    /* clear field for start */
    clearField();

    var getDataset = document.getElementById('getDataset');
    getDataset.addEventListener("click", function(e) {
        readInitialValues();
        
        /* show initial dataset */
        showField(dataset, true);

        /* bind solving to button click */
        var solveBtn = document.getElementById('solveBtn');

        getDataset.style.display = "none";
        solveBtn.style.display = "inherit";

        solveBtn.addEventListener("click", function(e) {
            solve();
        })
    })
});


function solve() {
    /* 
        initialize the automatic solving process by looking 
        for probable values for the first time 
    */
   checkProbableValues();

    /* from here on, loop the automatic solving process */
    while(hasUniqueProbableValues() === true) {
        fillProbableValues();
        checkProbableValues();
    }

    /* 
        we are done with the first two steps to solve a sudoku,
        which handled the "formal" solving parts. now it's time
        for bruteforcing the other values.
    */

}


/* check for probable values on the playing field */
function checkProbableValues() {
    dataset.forEach(function(row, rowindex) {
        row.forEach(function(value, colindex) {
            /* 0 indicates an empty field, check which numbers we can place here */
            var quad = whichQuadrant(rowindex, colindex);
            if(value === 0) {
                for(var i = 1; i <= 9; i++) {
                    if(checkRow(i, rowindex) === true) {
                        if(checkCol(i, colindex) === true) {
                            if(checkQuadrant(i, quad) === true) {
                                probableValues[rowindex][colindex].push(i);
                            }
                        }
                    }
                }
            }
        })
    })
}

/* check if there are unique probable values */
function hasUniqueProbableValues() {
    var hasUniques = false;
    
    /* are there probable values which are unique to a row or col? */
    probableValues.forEach(function(row, rowindex) {
        row.forEach(function(values, colindex) {
            if(values.length === 1) {
                hasUniques = true;
            }
        })
    })

    /* are there probable values which are unique in their quadrant? */
    for(var q = 0; q <= 8; q++) {
        for(var v = 1; v <= 9; v++) {
            if(hasUniquePositionInQuadrant(v, q).length === 1) {
                hasUniques = true;   
            }
        }
    }

    return hasUniques;
}

/* check if a probable value is unique in the quadrant */
function hasUniquePositionInQuadrant(value, quadrant) {
    var pos = [];

    /* quadrant to check */
    var qCoords = quadrants[quadrant];

    /* loop over the coordinates in the quadrant */
    for(var r = qCoords[0][0]; r <= qCoords[0][1]; r++) {
        for(var c = qCoords[1][0]; c <= qCoords[1][1]; c++) {
            probableValues[r][c].forEach(function(val, index) {
                if(val === value) {
                    pos.push([r, c]);
                }
            })
        }
    }

    /* return the unique positon */
    return pos;
}

/* fill probable values where there's only one possibility */
function fillProbableValues() {
    probableValues.forEach(function(row, rowindex) {
        row.forEach(function(values, colindex) {
            /* fields which have only one possible value */
            if(values.length === 1) {
                dataset[rowindex][colindex] = values[0];
                setFieldValue(values[0], rowindex, colindex);
            }            
        })
    })

    /* 
        fields with values which are unique in this quadrant
        like in: "the only place for a 5 in this quadrant"
    */
    for(var q = 0; q <= 8; q++) {
        for(var v = 1; v <= 9; v++) {
            var pos = [];
            if(hasUniquePositionInQuadrant(v, q).length === 1) {
                pos = hasUniquePositionInQuadrant(v, q);
                var row = pos[0][0];
                var col = pos[0][1];
                /* only set if not yet present */
                if(dataset[row][col] !== v) {
                    dataset[row][col] = v;
                    setFieldValue(v, row, col);
                }   
            }
        }
    }

    /* clear probable values for next run */
    probableValues.forEach(function(row, rowindex) {
        row.forEach(function(values, colindex) {
            probableValues[rowindex][colindex] = [];           
        })
    })
    
}


/* basic check functions */

/* check whether a value is allowed in a specific row */
function checkRow(value, row) {
    var isAllowed = true;

    dataset[row].forEach(function(val) {
        if(val === value) {
            isAllowed = false;
        }
    })

    return isAllowed;
}

/* check whether a value is allowed in a specific col */
function checkCol(value, col) {
    var isAllowed = true;

    for(var i = 0; i < 9; i++) {
        if(dataset[i][col] === value) {
            isAllowed = false;
        }
    }

    return isAllowed;
}

/* check whether a value is allowed in a specific quadrant */
function checkQuadrant(value, quadrant) {
    var isAllowed = true;

    /* quadrant to check */
    var qCoords = quadrants[quadrant];

    /* loop over the coordinates in the quadrant */
    for(var r = qCoords[0][0]; r <= qCoords[0][1]; r++) {
        for(var c = qCoords[1][0]; c <= qCoords[1][1]; c++) {
            if(getValue(r, c) === value) {
                isAllowed = false;
            }
        }
    }

    return isAllowed;
}

/* helper functions */

/* get value for a given X Y location in the dataset */
function getValue(row, col) {
    return dataset[row][col];
}

/* find quadrant for a given X Y location */
function whichQuadrant(row, col) {
    /*
        we define quadrants starting with 0 (upper left 3x3) to 8 (lower right 3x3),
        therefore multiplication operations can be used to find the quadrant
    */
    var quad = 0;
    var x = 0;
    var y = 0;

    if(row <= 2 && col <= 2) { quad = 0; }
    if(row >= 3 && row <= 5 && col <= 2) { quad = 3; }
    if(row >= 6 && col <= 2) { quad = 6; }

    if(row <= 2 && col >= 3 && col <= 5) { quad = 1; }
    if(row >= 3 && row <= 5 && col >= 3 && col <= 5) { quad = 4; }
    if(row >= 6 && col >= 3 && col <= 5) { quad = 7; }

    if(row <= 2 && col >= 6) { quad = 2; }
    if(row >= 3 && row <= 5 && col >= 6) { quad = 5; }
    if(row >= 6 && col >= 6) { quad = 8; }

    return quad;
}

/* clear the playing field */
function clearField() {
    var field = document.getElementById('playingField');
    for(var r = 0; r <= 8; r++) {
        for(var c = 0; c <= 8; c++) {
            field.children[r].children[c].value = '';
            field.children[r].children[c].className = '';
            field.children[r].children[c].readOnly = false;
        }
    }
}

/* read filled values from the playing field as initial data for the dataset */
function readInitialValues() {
    var field = document.getElementById('playingField');
    dataset.forEach(function(row, rowindex) {
        row.forEach(function(value, colindex) {
            if(field.children[rowindex].children[colindex].value.length == 1) {
                dataset[rowindex][colindex] = parseInt(field.children[rowindex].children[colindex].value);
            } else {
                dataset[rowindex][colindex] = 0;
            }
            
        })
    })
    console.log(dataset);
}

/* display the dataset on the playing field */
function showField(ds, preset = false) {
    var field = document.getElementById('playingField');
    ds.forEach(function(row, rowindex) {
            row.forEach(function(value, colindex) {
                /* do not process 0, it's just a placeholder */
                if(value !== 0) {
                    field.children[rowindex].children[colindex].value = value;
                    if(preset === true) {
                        field.children[rowindex].children[colindex].className ="preset";
                        field.children[rowindex].children[colindex].readOnly = true;
                    }
                }
            })
    })
}

/* add a value to the playing field */
function setFieldValue(val, row, col) {
    var field = document.getElementById('playingField');
    field.children[row].children[col].value = val;
=======
/*
    How to solve a Sudoku (programmatically)

    Sudokus have a simple ruleset to begin with:

    * the playing field is 9x9 fields in size
    * only numbers from 1 to 9 are allowed
    * each number must be unique to a row
    * each number must be unique to a col
    * each number must be unique to a quadrant (an 3x3 area)
     
    This ruleset can be used to build a list with allowed values 
    for each coordinate on the playing field in the beginning.

    If you find a unique value for a coordinate, enter it and
    re-run this list. This narrows down which numbers can/need
    to be placed.

    The next step is to look which values can only be placed in
    a unique position in a quadrant. As there's no other place
    for that value - but it needs to be present - this can be set, too.

    After having done those two steps till no more unique values
    are found, there's another step: You could look at a row/col and 
    quadrant combination. If there's a quadrant (q.0) with multiple 
    possible places for a value (p.0.0, p.2.1), say 5, and there's 
    another quadrant (q.1) where those places are all in a single 
    row/col (p.0.6, p.0.7), the unique placement options from q1 will 
    invalidate the placement option p.0.0 from q.0.

    This rule, which is "rather easy" to do by hand is skipped in
    this implementation. Instead it focuses directly on bruteforcing
    to find the missing placements.

*/

/* will hold the game (or the values of it) */
var dataset = [
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],

    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],

    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],]
]

/* this is a dummy dataset for testing */
/*
var dataset = [
    [0,0,0, 0,0,7, 9,0,0],
    [0,0,0, 0,0,0, 0,0,0],
    [0,9,1, 2,0,5, 0,8,0],

    [0,0,0, 4,0,0, 0,2,0],
    [2,7,0, 0,8,3, 4,0,9],
    [0,1,5, 0,0,0, 0,0,3],

    [0,0,9, 0,0,0, 0,0,0],
    [0,5,4, 0,0,2, 0,0,1],
    [0,0,0, 5,7,8, 0,0,0]
]
*/

/* 
    define the lower and upper limits for looping over quadrant positions
    these are coordinate pairs: [row, row], [col, col]
*/
const quadrants = [
    [[0,2], [0,2]],
    [[0,2], [3,5]],
    [[0,2], [6,8]],
    
    [[3,5], [0,2]],
    [[3,5], [3,5]],
    [[3,5], [6,8]],
    
    [[6,8], [0,2]],
    [[6,8], [3,5]],
    [[6,8], [6,8]]
]

/* structure to hold probable values */
var probableValues = [
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],

    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],

    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],],
    [[],[],[], [],[],[], [],[],[],]
]



document.addEventListener("DOMContentLoaded", (event) => {
    
    /* clear field for start */
    clearField();

    var getDataset = document.getElementById('getDataset');
    getDataset.addEventListener("click", function(e) {
        readInitialValues();
        
        /* show initial dataset */
        showField(dataset, true);

        /* bind solving to button click */
        var solveBtn = document.getElementById('solveBtn');

        getDataset.style.display = "none";
        solveBtn.style.display = "inherit";

        solveBtn.addEventListener("click", function(e) {
            solve();
        })
    })
});


function solve() {
    /* 
        initialize the automatic solving process by looking 
        for probable values for the first time 
    */
   checkProbableValues();

    /* from here on, loop the automatic solving process */
    while(hasUniqueProbableValues() === true) {
        fillProbableValues();
        checkProbableValues();
    }

    /* 
        we are done with the first two steps to solve a sudoku,
        which handled the "formal" solving parts. now it's time
        for bruteforcing the other values.
    */

}


/* check for probable values on the playing field */
function checkProbableValues() {
    dataset.forEach(function(row, rowindex) {
        row.forEach(function(value, colindex) {
            /* 0 indicates an empty field, check which numbers we can place here */
            var quad = whichQuadrant(rowindex, colindex);
            if(value === 0) {
                for(var i = 1; i <= 9; i++) {
                    if(checkRow(i, rowindex) === true) {
                        if(checkCol(i, colindex) === true) {
                            if(checkQuadrant(i, quad) === true) {
                                probableValues[rowindex][colindex].push(i);
                            }
                        }
                    }
                }
            }
        })
    })
}

/* check if there are unique probable values */
function hasUniqueProbableValues() {
    var hasUniques = false;
    
    /* are there probable values which are unique to a row or col? */
    probableValues.forEach(function(row, rowindex) {
        row.forEach(function(values, colindex) {
            if(values.length === 1) {
                hasUniques = true;
            }
        })
    })

    /* are there probable values which are unique in their quadrant? */
    for(var q = 0; q <= 8; q++) {
        for(var v = 1; v <= 9; v++) {
            if(hasUniquePositionInQuadrant(v, q).length === 1) {
                hasUniques = true; 
            }
        }
    }

    return hasUniques;
}

/* check if a probable value is unique in the quadrant */
function hasUniquePositionInQuadrant(value, quadrant) {
    var pos = [];

    /* quadrant to check */
    var qCoords = quadrants[quadrant];

    /* loop over the coordinates in the quadrant */
    for(var r = qCoords[0][0]; r <= qCoords[0][1]; r++) {
        for(var c = qCoords[1][0]; c <= qCoords[1][1]; c++) {
            probableValues[r][c].forEach(function(val, index) {
                if(val === value) {
                    pos.push([r, c]);
                }
            })
        }
    }

    /* return the unique positon */
    return pos;
}

/* fill probable values where there's only one possibility */
function fillProbableValues() {
    probableValues.forEach(function(row, rowindex) {
        row.forEach(function(values, colindex) {
            /* fields which have only one possible value */
            if(values.length === 1) {
                dataset[rowindex][colindex] = values[0];
                setFieldValue(values[0], rowindex, colindex);
            }            
        })
    })

    /* 
        fields with values which are unique in this quadrant
        like in: "the only place for a 5 in this quadrant"
    */
    for(var q = 0; q <= 8; q++) {
        for(var v = 1; v <= 9; v++) {
            var pos = [];
            if(hasUniquePositionInQuadrant(v, q).length === 1) {
                pos = hasUniquePositionInQuadrant(v, q);
                var row = pos[0][0];
                var col = pos[0][1];
                /* only set if not yet present */
                if(dataset[row][col] !== v) {
                    dataset[row][col] = v;
                    setFieldValue(v, row, col);
                }   
            }
        }
    }

    /* clear probable values for next run */
    probableValues.forEach(function(row, rowindex) {
        row.forEach(function(values, colindex) {
            probableValues[rowindex][colindex] = [];           
        })
    })
    
}


/* basic check functions */

/* check whether a value is allowed in a specific row */
function checkRow(value, row) {
    var isAllowed = true;

    dataset[row].forEach(function(val) {
        if(val === value) {
            isAllowed = false;
        }
    })

    return isAllowed;
}

/* check whether a value is allowed in a specific col */
function checkCol(value, col) {
    var isAllowed = true;

    for(var i = 0; i < 9; i++) {
        if(dataset[i][col] === value) {
            isAllowed = false;
        }
    }

    return isAllowed;
}

/* check whether a value is allowed in a specific quadrant */
function checkQuadrant(value, quadrant) {
    var isAllowed = true;

    /* quadrant to check */
    var qCoords = quadrants[quadrant];

    /* loop over the coordinates in the quadrant */
    for(var r = qCoords[0][0]; r <= qCoords[0][1]; r++) {
        for(var c = qCoords[1][0]; c <= qCoords[1][1]; c++) {
            if(getValue(r, c) === value) {
                isAllowed = false;
            }
        }
    }

    return isAllowed;
}

/* helper functions */

/* get value for a given X Y location in the dataset */
function getValue(row, col) {
    return dataset[row][col];
}

/* find quadrant for a given X Y location */
function whichQuadrant(row, col) {
    /*
        we define quadrants starting with 0 (upper left 3x3) to 8 (lower right 3x3),
        therefore multiplication operations can be used to find the quadrant
    */
    var quad = 0;
    var x = 0;
    var y = 0;

    if(row <= 2 && col <= 2) { quad = 0; }
    if(row >= 3 && row <= 5 && col <= 2) { quad = 3; }
    if(row >= 6 && col <= 2) { quad = 6; }

    if(row <= 2 && col >= 3 && col <= 5) { quad = 1; }
    if(row >= 3 && row <= 5 && col >= 3 && col <= 5) { quad = 4; }
    if(row >= 6 && col >= 3 && col <= 5) { quad = 7; }

    if(row <= 2 && col >= 6) { quad = 2; }
    if(row >= 3 && row <= 5 && col >= 6) { quad = 5; }
    if(row >= 6 && col >= 6) { quad = 8; }

    return quad;
}

/* clear the playing field */
function clearField() {
    var field = document.getElementById('playingField');
    for(var r = 0; r <= 8; r++) {
        for(var c = 0; c <= 8; c++) {
            field.children[r].children[c].value = '';
            field.children[r].children[c].className = '';
            field.children[r].children[c].readOnly = false;
        }
    }
}

/* read filled values from the playing field as initial data for the dataset */
function readInitialValues() {
    var field = document.getElementById('playingField');
    dataset.forEach(function(row, rowindex) {
        row.forEach(function(value, colindex) {
            if(field.children[rowindex].children[colindex].value.length == 1) {
                dataset[rowindex][colindex] = parseInt(field.children[rowindex].children[colindex].value);
            } else {
                dataset[rowindex][colindex] = 0;
            }
            
        })
    })
    console.log(dataset);
}

/* display the dataset on the playing field */
function showField(ds, preset = false) {
    var field = document.getElementById('playingField');
    ds.forEach(function(row, rowindex) {
            row.forEach(function(value, colindex) {
                /* do not process 0, it's just a placeholder */
                if(value !== 0) {
                    field.children[rowindex].children[colindex].value = value;
                    if(preset === true) {
                        field.children[rowindex].children[colindex].className ="preset";
                        field.children[rowindex].children[colindex].readOnly = true;
                    }
                }
            })
    })
}

/* add a value to the playing field */
function setFieldValue(val, row, col) {
    var field = document.getElementById('playingField');
    field.children[row].children[col].value = val;
>>>>>>> a842c4f6e9a15747fa5ed56f21d26a82e0f119e5
}